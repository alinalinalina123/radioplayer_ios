//
//  Radio.m
//  RadioPlayer
//
//  Created by mac-242 on 3/27/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

#import "Radio.h"
#import <UIKit/UIKit.h>
@implementation Radio

- (id)initWithURL:(NSString *)url andTitle:(NSString *)title andImage:(UIImage *)image {
    
    self = [super init];
    
    if(self) {
        self.url = url;
        self.title = title;
        self.image = image;
    }
    
    return self;
}

@end
