//
//  Radio.h
//  RadioPlayer
//
//  Created by mac-242 on 3/27/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Radio : NSObject
@property (nonatomic,strong) NSString *url;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) UIImage *image;

-(id) initWithURL:(NSString *)url andTitle:(NSString *)title andImage:(UIImage *)image;
@end
