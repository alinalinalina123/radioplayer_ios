//
//  main.m
//  RadioPlayer
//
//  Created by mac-242 on 3/27/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
