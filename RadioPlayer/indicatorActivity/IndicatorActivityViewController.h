//
//  IndicatorActivityViewController.h
//  RadioPlayer
//
//  Created by mac-242 on 3/29/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndicatorActivityViewController : UIViewController
@property (nonatomic,strong) NSArray *arrayOfRadios;
- (void) navigateToTabelView;
@end
