//
//  IndicatorActivityViewController.m
//  RadioPlayer
//
//  Created by mac-242 on 3/29/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

#import "IndicatorActivityViewController.h"
#import "Networking.h"
#import "RadioTableViewController.h"
@interface IndicatorActivityViewController (){
    Networking *client;
}

@end

@implementation IndicatorActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    client = [[[[Networking alloc] init]retain]autorelease];
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    _arrayOfRadios = [client retriveRadioStations];
    [self navigateToTabelView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [_arrayOfRadios release];
    _arrayOfRadios = nil;
    [super viewDidUnload];
}

- (void)navigateToTabelView {
    RadioTableViewController *myVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RadioTabel"];
    myVC.arrayOfRadios = _arrayOfRadios;
    [self presentViewController:myVC animated:NO completion:nil];
}

- (void)dealloc {
    [_arrayOfRadios release];
    _arrayOfRadios = nil;
    [super dealloc];
}

@end
