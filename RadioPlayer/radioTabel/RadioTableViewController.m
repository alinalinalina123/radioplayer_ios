//
//  RadioTableViewController.m
//  RadioPlayer
//
//  Created by mac-242 on 3/27/18.
//  Copyright © 2018 mac-242. All rights reserved.
//
///JSON - https://api.myjson.com/bins/19jb47

#import "RadioTableViewController.h"
#import "RadioTableViewCell.h"
#import "Radio.h"
#import "ViewController.h"

@interface RadioTableViewController ()

@end

@implementation RadioTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arrayOfRadios count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self navigateToMyNewViewController:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     static NSString *MyIdentifier = @"RadioCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
    }
    cell.textLabel.text = [[_arrayOfRadios objectAtIndex:indexPath.row] title];
    return cell;
}

- (void)navigateToMyNewViewController:(NSInteger)selectedItemIndex {
    ViewController *myVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RadioDetail"];
    myVC.radioUrl = [_arrayOfRadios objectAtIndex:selectedItemIndex];
    [self presentViewController:myVC animated:NO completion:nil];
}

@end
