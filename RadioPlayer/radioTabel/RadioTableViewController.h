//
//  RadioTableViewController.h
//  RadioPlayer
//
//  Created by mac-242 on 3/27/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RadioTableViewController : UITableViewController
@property (nonatomic,strong) NSArray *arrayOfRadios;
@end
