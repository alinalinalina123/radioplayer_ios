//
//  Networking.m
//  RadioPlayer
//
//  Created by mac-242 on 3/30/18.
//  Copyright © 2018 mac-242. All rights reserved.
//
#define CONNECTION_TIMEOUT_SECONDS      2.0
#define CONNECTION_CHECK_INTERVAL       1
#import <Foundation/Foundation.h>
#import "Networking.h"
#import <AFNetworking.h>
#import "Radio.h"
@implementation Networking
NSTimer *timer;
BOOL timeout;

- init {
    self = [super init];
    if (self) {
        url = [[NSString alloc] initWithString:@"https://api.myjson.com/bins/19jb47"];
    }
    return self;
}

- (void)dealloc {
    [url release];
    [super dealloc];
}

-(void) startTimer {
    timeout = NO;
    [timer invalidate];
    timer = [NSTimer timerWithTimeInterval:CONNECTION_TIMEOUT_SECONDS target:self selector:@selector(connectionTimeout) userInfo:nil repeats:NO];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
}

-(void) stopTimer {
    [timer invalidate];
    timer = nil;
}

-(void) connectionTimeout {
    timeout = YES;
    [self stopTimer];
}

- (NSArray *) retriveRadioStations {
    
    NSMutableArray *radiosArray = [[NSMutableArray alloc]init];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:configuration];
    
    manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
   
    [manager GET:url parameters:nil progress:nil
    success:^(NSURLSessionTask *task, id responseObject) {
       [self startTimer];
       NSArray *array = responseObject;
       for (NSDictionary *dict  in array) {
           NSString *title = [dict valueForKey:@"title"];
           NSString *urlRadio = [dict valueForKey:@"url"];
           NSString *image = [dict valueForKey:@"image"];
           Radio *radioStation = [[Radio alloc] initWithURL: urlRadio andTitle:title andImage:[UIImage imageNamed:image]];
           [radiosArray addObject:radioStation];
           [radioStation release];
       }
        while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW)){
            if(timeout){
                dispatch_semaphore_signal(semaphore);
            }
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                     beforeDate:[NSDate dateWithTimeIntervalSinceNow:CONNECTION_CHECK_INTERVAL]];
        };
       
        [self stopTimer];
    }
    
    failure:^(NSURLSessionTask *operation, NSError *error) {
       NSLog(@"Error: %@", error);
       dispatch_semaphore_signal(semaphore);
    }];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    [semaphore release];
    [manager invalidateSessionCancelingTasks:YES];
    [manager release];
    
    return radiosArray;
}

@end
