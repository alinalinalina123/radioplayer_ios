//
//  ViewController.m
//  RadioPlayer
//
//  Created by mac-242 on 3/27/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

#import "ViewController.h"
#import "Radio.h"
@interface ViewController ()

@end

@implementation ViewController

@synthesize player;
int count;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSURL *url = [NSURL URLWithString:_radioUrl.url];
    player = [AVPlayer playerWithURL:url];
    
    player.currentItem.preferredForwardBufferDuration = 1;
    _navigationBar.title = _radioUrl.title;
    _radioLogo.image = _radioUrl.image;
    count = 0;
}

- (IBAction)btnPlay:(id)sender {
      if (count == 0) {
          count++;
          [player play];
      }
}

- (IBAction)btnStop:(id)sender {
    if(count == 1){
        count --;
        [player pause];
        [player.currentItem cancelPendingSeeks];
        [player.currentItem.asset cancelLoading];
    }
}
- (IBAction)backToList:(id)sender {
    player = nil;
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)changeVolume:(id)sender {
    player.volume = _volumeSlider.value;
    @autoreleasepool{
        if (_volumeSlider.value == 0) {
            _volumeImage.image = [UIImage imageNamed:@"ic_volume_off"];
        } else {
            _volumeImage.image = [UIImage imageNamed:@"ic_volume_up"];
        }
    }
}

@end
