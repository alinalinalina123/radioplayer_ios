//
//  ViewController.h
//  RadioPlayer
//
//  Created by mac-242 on 3/27/18.
//  Copyright © 2018 mac-242. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Radio.h"
@interface ViewController : UIViewController { 
}
@property (weak, nonatomic) IBOutlet UISlider *volumeSlider;
@property(nonatomic, strong) Radio *radioUrl;
@property (weak, nonatomic) IBOutlet UIImageView *volumeImage;
@property(nonatomic, retain) AVPlayer *player;
@property (weak, nonatomic) IBOutlet UIImageView *radioLogo;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationBar;
- (IBAction)btnPlay:(id)sender;
- (IBAction)btnStop:(id)sender;
- (IBAction)changeVolume:(id)sender;
- (IBAction)backToList:(id)sender;
@end

